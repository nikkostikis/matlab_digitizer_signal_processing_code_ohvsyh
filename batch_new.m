function batch_new(PDN,procdata,index,sampling)
%Put all data in a master table
%ID  L     R    E   F
%101 3.5       3.5
%101 2              2
%clear all
close all 

global IMAGES;
IMAGES=0;

global Hand;
global Direction;
global BDirection;


MT=zeros(30,5,100); %trial#(may be larger than 30), L/R/E/F/WorstHand, subject# 


%Comment out according to folder/populations
% if (strcmp(PDN,'PD')==1)
%     dirname='../../Data/PD'; synth='PD';
% 
% elseif (strcmp(PDN,'OH')==1)
%     dirname='../../Data/OH'; synth='H';
% 
% end

dirname=strcat('../../Data/', PDN); synth=PDN;

%In order to use the ProcessdData folder correctly, the appropriate files
%need to be copied from _x, _y or _xy folders

outfile_All=strcat('ProcessedData/',synth,'_All.mat');
outfile_Means=strcat('ProcessedData/',synth,'_Means.mat');
outfileH_All=strcat('ProcessedData/WorstHand_',synth,'_All.mat');
outfileHD_All=strcat('ProcessedData/WorstHandDirection_',synth,'_All.mat');
handfile=strcat('ProcessedData/WhichWorstHand_',synth,'.mat');
directionfile=strcat('ProcessedData/WhichWorstDirection_',synth,'.mat');
durationfile=strcat('ProcessedData/Duration_',synth,'.mat');
velocityfile=strcat('ProcessedData/Velocity_',synth,'.mat');

Bdirectionfile=strcat('ProcessedData/WhichBestHandandDirection_',synth,'.mat');

allDatafile=strcat('ProcessedData/AllData_',synth,'.mat');

durationVec=[];
meanvelVec=[];

D=dir(dirname);
D=D(3:length(D));


mfile=strcat('MEANS',synth,'.txt');
mfid=fopen(mfile,'w+');
fprintf(mfid,'shortname QindexAll QindexWorstHand WorstHandID\n'); % QindexD DID\n');

sumQindex=0;
nfiles=0;
pcount=0;
shortname=' ';
fcount=1;

for j=1:length(D);
    j
    fname=D(j).name;
    fcount;
    nfiles;
    if ~strcmp(fname,'.DS_Store')
        idx=find(fname=='_');
        numcode=str2num(fname(2:idx-1));


        %save data for last batch if we are starting a new one
        idx=find(fname=='_');
        if (~strcmp(shortname,fname(1:idx-1))) %we have a new class of filenames
            fname(1:idx-1)
            %save current mean
            if (fcount~=1)
                if (sumQindexL/Lfiles>sumQindexR/Rfiles)
                    meanHandindex=sumQindexL/Lfiles;
                    Handid=1; %means left side
                    fprintf(mfid,'%s %f %f %d\n',shortname, sumQindex/nfiles, meanHandindex, Handid);

                else
                    meanHandindex=sumQindexR/Rfiles;
                    Handid=-1; %means right side
                    fprintf(mfid,'%s %f %f %d\n',shortname, sumQindex/nfiles, meanHandindex, Handid);
                end

                MT(nfiles,5,pcount)=Handid; %1=Left, -1=Right
            end
            %redefine firstname/zero out sum.
            shortname=fname(1:idx-1);
            sumQindex=0;
            sumQindexL=0;
            sumQindexR=0;
            sumQindexE=0;
            sumQindexF=0;
            nfiles=0;
            Lfiles=0;
            Rfiles=0;
            Efiles=0;
            Ffiles=0;
            pcount=pcount+1;
        end

        %get next filename and compute the score
        dname2=strcat(dirname,'/');
        longfname=strcat(dname2,fname);



        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%
        %%%
%       [ii, maxfindex, speed, ex, vx, maxerrx, vel, Qindex]=basic(longfname);  
        [Qindex,duration,meanvel]=basic_new(longfname,synth,index, sampling);  
        %%%
        %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        durationVec(end+1)=duration;
        meanvelVec(end+1)=meanvel;
        
        metric=Qindex;

        sumQindex=sumQindex+Qindex;
        nfiles=nfiles+1;

        %determine where to store result
        if (~isempty(strfind(fname,'_L_')) && isempty(strfind(fname,'_R_')))
            MT(nfiles,1,pcount)=metric;
            Lfiles=Lfiles+1;
            %Handid=1; %means left side
            sumQindexL=sumQindexL+Qindex;
        elseif (~isempty(strfind(fname,'_R_')) && isempty(strfind(fname,'_L_')))
            MT(nfiles,2,pcount)=metric;
            Rfiles=Rfiles+1;
            %Handid=-1; %means right side
            sumQindexR=sumQindexR+Qindex;
        else
            fprintf('ooops... neither L nor R! \n');
            pause;
        end

        if (~isempty(strfind(fname,'_E_')) && isempty(strfind(fname,'_F_')))
%                 if (~isempty(strfind(fname,'_R_')) && isempty(strfind(fname,'_L_')))
                MT(nfiles,3,pcount)=metric;
                Efiles=Efiles+1;
                %Dircid=2; %means extension
                sumQindexE=sumQindexE+Qindex;
 
        elseif (~isempty(strfind(fname,'_F_')) && isempty(strfind(fname,'_E_')))
                MT(nfiles,4,pcount)=metric;
                Ffiles=Ffiles+1;
                %Dircid=-2; %means flexion
                sumQindexF=sumQindexF+Qindex;

        else
            fprintf('ooops... neither E nor F! \n');
            pause;
        end

        fcount=fcount+1;
    end
end
% fclose(fid);

%save mean for last file

if (sumQindexL/Lfiles>sumQindexR/Rfiles)
    meanHandindex=sumQindexL/Lfiles;
    Handid=1; %means left side
    fprintf(mfid,'%s %f %f %d\n',shortname, sumQindex/nfiles, meanHandindex, Handid);
else
    meanHandindex=sumQindexR/Rfiles;
    Handid=-1; %means right side
    fprintf(mfid,'%s %f %f %d\n',shortname, sumQindex/nfiles, meanHandindex, Handid);
end

MT(nfiles,5,pcount)=Handid; %1=Left, -1=Right


fclose(mfid);
fclose all;


globL=[];
globR=[];
globE=[];
globF=[];

globLE=[];
globRE=[];
globLF=[];
globRF=[];

globHH=[];
globHE=[];
globHF=[];
globHB=[];
globHBE=[];
globHBF=[];

globHD=[];
means=zeros(pcount,12); 
Hand=zeros(pcount,2);
Direction=zeros(pcount,2);
BDirection=zeros(pcount,3); %includes index for best hand
for i=1:pcount %for each person
    aa=MT(:,:,i); %isolate their data
    idxL=find(aa(:,1)~=0);
    idxR=find(aa(:,2)~=0);
    idxE=find(aa(:,3)~=0);
    idxF=find(aa(:,4)~=0);
    Ldata=aa(idxL,1);
    Rdata=aa(idxR,2);
    Edata=aa(idxE,3);
    Fdata=aa(idxF,4);
    idh=find(aa(:,5)~=0);
    Hand(i,1)=i;
    Hand(i,2)=aa(idh,5); %1:Left, -1:Right

    id1=find((aa(:,1)==aa(:,3)));
    id2=find((aa(:,1)~=0));
    idx=intersect(id1,id2);
    LEdata=aa(idx,1);

    id1=find((aa(:,1)==aa(:,4)));
    id2=find((aa(:,1)~=0));
    idx=intersect(id1,id2);
    LFdata=aa(idx,1);

    id1=find((aa(:,2)==aa(:,3)));
    id2=find((aa(:,2)~=0));
    idx=intersect(id1,id2);
    REdata=aa(idx,2);

    id1=find((aa(:,2)==aa(:,4)));
    id2=find((aa(:,2)~=0));
    idx=intersect(id1,id2);
    RFdata=aa(idx,2);
   
    %XXX 1: try mean, worst case, worst 3, etc...
    means(i,1:11)=[mean([Ldata; Rdata]), mean(Ldata), mean(Rdata), mean(Edata), mean(Fdata), mean(LEdata),mean(LFdata),mean(REdata),mean(RFdata),mean([Edata;Fdata]), mean([Rdata;LFdata])];
   
    %%append to global list%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    globL=[globL; Ldata];
    globR=[globR; Rdata];
    globE=[globE; Edata];
    globF=[globF; Fdata];

    globLE=[globLE; LEdata];
    globLF=[globLF; LFdata];
    globRE=[globRE; REdata];
    globRF=[globRF; RFdata];
    
    allData{i,1}=[Ldata;Rdata];
    allData{i,2}=Ldata;
    allData{i,3}=Rdata;
    allData{i,4}=Edata;
    allData{i,5}=Fdata;
    allData{i,6}=LEdata;
    allData{i,7}=LFdata;
    allData{i,8}=REdata;
    allData{i,9}=LFdata;
    
    
    if (Hand(i,2)==1)%worst hand: Left
        globHH=[globHH; Ldata];
        globHE=[globHE; LEdata];
        globHF=[globHF; LFdata];
        
        globHB=[globHB; Rdata];
        globHBE=[globHBE; REdata];
        globHBF=[globHBF; RFdata];
        
        allData{i,10}=Ldata;
        allData{i,11}=Rdata;

        %Determine worst direction for worst hand, here L
        if (nanmean(LFdata)>nanmean(LEdata))%worst direction: L FLEXION
            globHD=[globHD; LFdata];
            allData{i,12}=LFdata;
            Direction(i,1)=i;
            Direction(i,2)=-2;
            means(i,12)=mean([LEdata;Rdata]);
        else %worst direction: L EXTENSION
            globHD=[globHD; LEdata];
            allData{i,12}=LEdata;
            Direction(i,1)=i;
            Direction(i,2)=2;
            means(i,12)=mean([LFdata;Rdata]);
        end
        
        %Determine best direction for best hand, here best hand is R
        if (nanmean(RFdata)>nanmean(REdata))%best direction: R EXTENSION, marked as 2
            BDirection(i,1)=i;
            BDirection(i,2)=2;
            BDirection(i,3)=-1; %R best hand
        else %best direction: R FLEXION, marked as -2
            BDirection(i,1)=i;
            BDirection(i,2)=-2;
            BDirection(i,3)=-1; %R best hand
        end
        
    else %worst hand: Right
        globHH=[globHH; Rdata];
        globHE=[globHE; REdata];
        globHF=[globHF; RFdata];
        globHB=[globHB; Ldata];
        allData{i,10}=Rdata;
        allData{i,11}=Ldata;

        %Determine worst direction for worst hand, here R 
        if (nanmean(RFdata)>nanmean(REdata))%worst direction: R FLEXION
            globHD=[globHD; RFdata];
            allData{i,12}=RFdata;
            Direction(i,1)=i;
            Direction(i,2)=-2;
            means(i,12)=mean([REdata;Ldata]);
        else %worst direction: R EXTENSION
            globHD=[globHD; REdata];
            allData{i,12}=REdata;
            Direction(i,1)=i;
            Direction(i,2)=2;
            means(i,12)=mean([RFdata;Ldata]);
        end 
        
        
        %Determine best direction for best hand, here best hand is L
        if (nanmean(LFdata)>nanmean(LEdata))%best direction: L EXTENSION, marked as 2
            BDirection(i,1)=i;
            BDirection(i,2)=2;
            BDirection(i,3)=1;
        else %best direction: L FLEXION, marked as -2
            BDirection(i,1)=i;
            BDirection(i,2)=-2;
            BDirection(i,3)=1;
        end
        
    end

end

%save global structure to a file or means matrix to same file
glob{1}=globL;
glob{2}=globR;
glob{3}=globE;
glob{4}=globF;
glob{5}=globLE;
glob{6}=globLF;
glob{7}=globRE;
glob{8}=globRF;
glob{9}=[globL;globR];
glob{10}=[globE;globF];
glob{11}=[globR;globLF];
%save worst hand structure to a file
globH{1}=globHH;
globH{2}=globHE;
globH{3}=globHF;
globH{4}=globHB;
%save worst direction of worst hand to a file (it's an array, not a cell, it's 1 col)


H=strcat('Hand_',synth);
D=strcat('Direction_',synth);

BD=strcat('BDirection_',synth);

gH=strcat('globH_',synth);
gHD=strcat('globHD_',synth);
allD=strcat('allData_',synth);
if (strcmp(procdata,'MEANS'))
    Gstr=strcat('globM_',synth);
    eval(strcat(Gstr,'=means;'));
    eval(['save',' ',outfile_Means,' ',Gstr]);
else
    Gstr=strcat('globA_',synth);
    eval(strcat(Gstr,'=glob;'));
    eval(['save',' ',outfile_All,' ',Gstr]);
    eval(strcat(gH,'=globH;'));
    eval(['save',' ',outfileH_All,' ',gH]);
    eval(strcat(gHD,'=globHD;'));
    eval(['save',' ',outfileHD_All,' ',gHD]);
end
eval(strcat(H,'=Hand;'));
eval(['save',' ',handfile,' ',H]);
eval(strcat(D,'=Direction;'));
eval(['save',' ',directionfile,' ',D]);

eval(strcat(BD,'=BDirection;'));
eval(['save',' ',Bdirectionfile,' ',BD]);

eval(strcat(allD,'=allData;'));
eval(['save',' ',allDatafile,' ',allD]);

dur=strcat('Duration_',synth);
eval(strcat(dur,'=durationVec;'));
eval(['save',' ',durationfile,' ',dur]);%duration for all files

vel=strcat('Velocity_',synth);
eval(strcat(vel,'=meanvelVec;'));
eval(['save',' ',velocityfile,' ',vel]);%velocity for all files


return;