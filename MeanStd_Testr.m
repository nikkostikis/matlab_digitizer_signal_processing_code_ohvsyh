FilesReference
MeanValues=zeros(32,1);
Stds=zeros(32,1);

fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

fprintf('\nMeanStd for MEANS\n\n');
[MeanValues(1,1) Stds(1,1)]=meanStd(globM_H(:,1),'allh N');
[MeanValues(2,1) Stds(2,1)]=meanStd(glob2_H(:,1),'wh N');
[MeanValues(3,1) Stds(3,1)]=meanStd(glob2_H(:,4),'bh N');
[MeanValues(4,1) Stds(4,1)]=meanStd(glob4_H,'wd N');
[MeanValues(5,1) Stds(5,1)]=meanStd(globM_H(:,5),'F N');
[MeanValues(6,1) Stds(6,1)]=meanStd(globM_H(:,4),'E N');
[MeanValues(7,1) Stds(7,1)]=meanStd(globM_H(:,2),'L N');
[MeanValues(8,1) Stds(8,1)]=meanStd(globM_H(:,3),'R N');

fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
[MeanValues(9,1) Stds(9,1)]=meanStd(globM_PD(:,1),'allh PD');
[MeanValues(10,1) Stds(10,1)]=meanStd(glob2_PD(:,1),'wh PD');
[MeanValues(11,1) Stds(11,1)]=meanStd(glob2_PD(:,4),'bh PD');
[MeanValues(12,1) Stds(12,1)]=meanStd(glob4_PD,'wd PD');
[MeanValues(13,1) Stds(13,1)]=meanStd(globM_PD(:,5),'f PD');
[MeanValues(14,1) Stds(14,1)]=meanStd(globM_PD(:,4),'e PD');
[MeanValues(15,1) Stds(15,1)]=meanStd(globM_PD(:,2),'L PD');
[MeanValues(16,1) Stds(16,1)]=meanStd(globM_PD(:,3),'R PD');


fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nMeanStd for ALL\n\n');
[MeanValues(17,1) Stds(17,1)]=meanStd(globA_H{9},'allh N');
[MeanValues(18,1) Stds(18,1)]=meanStd(globH_H{1},'wh N');
[MeanValues(19,1) Stds(19,1)]=meanStd(globH_H{4},'bh N');
[MeanValues(20,1) Stds(20,1)]=meanStd(globHD_H,'wd N');
[MeanValues(21,1) Stds(21,1)]=meanStd(globA_H{4},'f N');
[MeanValues(22,1) Stds(22,1)]=meanStd(globA_H{3},'e N');
[MeanValues(23,1) Stds(23,1)]=meanStd(globA_H{1},'L N');
[MeanValues(24,1) Stds(24,1)]=meanStd(globA_H{2},'R N');

fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
[MeanValues(25,1) Stds(25,1)]=meanStd(globA_PD{9},'allh PD');
[MeanValues(26,1) Stds(26,1)]=meanStd(globH_PD{1},'wh PD');
[MeanValues(27,1) Stds(27,1)]=meanStd(globH_PD{4},'bh PD');
[MeanValues(28,1) Stds(28,1)]=meanStd(globHD_PD,'wd PD');
[MeanValues(29,1) Stds(29,1)]=meanStd(globA_PD{4},'f PD');
[MeanValues(30,1) Stds(30,1)]=meanStd(globA_PD{3},'e PD');
[MeanValues(31,1) Stds(31,1)]=meanStd(globA_PD{1},'L PD');
[MeanValues(32,1) Stds(32,1)]=meanStd(globA_PD{2},'R PD');

[MeanValues(33,1) Stds(33,1)]=meanStd(glob3_H(:,1),'wh-bh N');
[MeanValues(34,1) Stds(34,1)]=meanStd(glob3_PD(:,1),'wh-bh PD');
