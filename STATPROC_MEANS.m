%STATISTICAL PROCESSING

%read data from ProcessedData directory and run statistical tests
clear all;


load 'ProcessedData/YH_Means.mat'
load 'ProcessedData/OH_Means.mat'
%there is 1 matrix, globM_YH (Healthy) and globM_OH (OH)
%each contains 1 row per subject, 11 columns in the order
%ALL, L R E F LE LF RE RF EF R+LF

load 'ProcessedData/WhichWorstHand_YH.mat'
load 'ProcessedData/WhichWorstHand_OH.mat'
%there is 1 matrix Hand_YH and Hand_OH, each contains
%1 row per subject and 2 columns, 1st numOfSubject (redundant)
%2nd 1 for left, -1 for right worst (best) hand

load 'ProcessedData/WhichWorstDirection_YH.mat'
load 'ProcessedData/WhichWorstDirection_OH.mat'
%there is 1 matrix Direction_YH and Direction_OH, each contains
%1 row per subject and 2 columns, 1st numOfSubject (redundant)
%2nd 2 for extension, -2 for flexion as worst direction of movement

load 'ProcessedData/WhichBestHandandDirection_YH.mat'
load 'ProcessedData/WhichBestHandandDirection_OH.mat'
%there is 1 matrix BDirection_YH and BDirection_OH, each contains
%1 row per subject and 3 columns, 1st numOfSubject (redundant)
%2nd 2 for extension, -2 for flexion as best direction of movement
%3rd 1 for left, -1 for right as best hand


%Get current folder to save figure later on
tempF=pwd; 
idxF=find(tempF=='/');
idxF=max(idxF);
folder=tempF(idxF+1:length(tempF));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




fprintf('\nSTATPROC_MEANS is running---------------------------\n');

pcount_YH=size(globM_YH);
pcount_OH=size(globM_OH);
for i=1:pcount_YH
    if  (Hand_YH(i,2)==1) %Left hand worst
        glob2_YH(i,:)=[globM_YH(i,2),globM_YH(i,6),globM_YH(i,7),globM_YH(i,3)];
        glob3_YH(i,:)=[globM_YH(i,2)-globM_YH(i,3),...
                        globM_YH(i,6)-globM_YH(i,8),...
                            globM_YH(i,7)-globM_YH(i,9)];
        if (Direction_YH(i,2)==2) %Keep Left Extension
            glob4_YH(i,:)=globM_YH(i,6);
            glob5_YH(i,:)=globM_YH(i,6)-globM_YH(i,7); %wrong because I have (worst hand&best direction), which doesn't work
        else %Keep Left Flexion
            glob4_YH(i,:)=globM_YH(i,7);
            glob5_YH(i,:)=globM_YH(i,7)-globM_YH(i,6);
        end
    else %Right hand worst
        glob2_YH(i,:)=[globM_YH(i,3),globM_YH(i,8),globM_YH(i,9),globM_YH(i,2)];
        glob3_YH(i,:)=[globM_YH(i,3)-globM_YH(i,2),...
                        globM_YH(i,8)-globM_YH(i,6),...
                            globM_YH(i,9)-globM_YH(i,7)];
        if (Direction_YH(i,2)==2) %Keep Right Extension
            glob4_YH(i,:)=globM_YH(i,8);
            glob5_YH(i,:)=globM_YH(i,8)-globM_YH(i,9);
        else %Keep Right Flexion
            glob4_YH(i,:)=globM_YH(i,9);
            glob5_YH(i,:)=globM_YH(i,9)-globM_YH(i,8);
        end
    end
    
    %Best Direction segment
    if (BDirection_YH(i,2)==2) %best direction Ext
        if (BDirection_YH(i,3)==1) %best hand L
            glob6_YH(i,:)=globM_YH(i,6); %LE
        else %best hand R
            glob6_YH(i,:)=globM_YH(i,8); %RE
        end
    else %best direction Flex
        if (BDirection_YH(i,3)==1) %best hand L
            glob6_YH(i,:)=globM_YH(i,7); %LF
        else %best hand R
            glob6_YH(i,:)=globM_YH(i,9); %RF
        end
    end
    %End of best direction segment
        
        
end

for i=1:pcount_OH
    if  (Hand_OH(i,2)==1) %Left Hand worst
        glob2_OH(i,:)=[globM_OH(i,2),globM_OH(i,6),globM_OH(i,7),globM_OH(i,3)];
        glob3_OH(i,:)=[globM_OH(i,2)-globM_OH(i,3),...
                        globM_OH(i,6)-globM_OH(i,8),...
                            globM_OH(i,7)-globM_OH(i,9)];
        if (Direction_OH(i,2)==2) %Keep Left Extension
            glob4_OH(i,:)=globM_OH(i,6);
            glob5_OH(i,:)=globM_OH(i,6)-globM_OH(i,7);
        else %Keep Left Flexion
            glob4_OH(i,:)=globM_OH(i,7);
            glob5_OH(i,:)=globM_OH(i,7)-globM_OH(i,6);
        end
    else %Right Hand worst
        glob2_OH(i,:)=[globM_OH(i,3),globM_OH(i,8),globM_OH(i,9),globM_OH(i,2)];
        glob3_OH(i,:)=[globM_OH(i,3)-globM_OH(i,2),...
                        globM_OH(i,8)-globM_OH(i,6),...
                            globM_OH(i,9)-globM_OH(i,7)];
        if (Direction_OH(i,2)==2) %Keep Right Extension
            glob4_OH(i,:)=globM_OH(i,8);
            glob5_OH(i,:)=globM_OH(i,8)-globM_OH(i,9);
        else %Keep Right Flexion
            glob4_OH(i,:)=globM_OH(i,9);
            glob5_OH(i,:)=globM_OH(i,9)-globM_OH(i,8);
        end
    end

    %Best Direction segment
    if (BDirection_OH(i,2)==2) %best direction Ext
        if (BDirection_OH(i,3)==1) %best hand L
            glob6_OH(i,:)=globM_OH(i,6); %LE
        else %best hand R
            glob6_OH(i,:)=globM_OH(i,8); %RE
        end
    else %best direction Flex
        if (BDirection_OH(i,3)==1) %best hand L
            glob6_OH(i,:)=globM_OH(i,7); %LF
        else %best hand R
            glob6_OH(i,:)=globM_OH(i,9); %RF
        end
    end
    %End of best direction segment
    
end
%glob2_YH contains 4 columns, L,LE,LF,R or R,RE,RF,L dependent on the the Hand_YH
%matrix, same goes for glob2_OH

%glob3_YH contains 3 columns L-R, LE-RE, LF-RF or R-L, RE-LE, RF-LF
%dependent on the Hand_YH matrix, same goes for glob3_OH

save('ProcessedData/WorstHand_YH_MEANS.mat','glob2_YH');%Mean values, Worst Hand x4
save('ProcessedData/WorstHand_OH_MEANS.mat','glob2_OH');
save('ProcessedData/WorstHandDirection_YH_MEANS.mat','glob4_YH');%Mean values, WorstHand Direction x1
save('ProcessedData/WorstHandDirection_OH_MEANS.mat','glob4_OH');
save('ProcessedData/DiafWorstBestHand_YH_MEANS.mat','glob3_YH');%Mean values, Worst-Best x3
save('ProcessedData/DiafWorstBestHand_OH_MEANS.mat','glob3_OH');
save('ProcessedData/DiafWorstBestHandDirection_YH_MEANS.mat','glob5_YH');%Mean values, WorstHand Direction-BestHand Direction x1
save('ProcessedData/DiafWorstBestHandDirection_OH_MEANS.mat','glob5_OH');
save('ProcessedData/BestDirection_YH_MEANS.mat', 'glob6_YH'); %mean data Best Direction x1
save('ProcessedData/BestDirection_OH_MEANS.mat', 'glob6_OH'); 


%Separation groups
globSep_YH=[glob2_YH(), globM_YH(:,1:9), glob4_YH(), glob3_YH(:,1), glob6_YH()];
globSep_OH=[glob2_OH(), globM_OH(:,1:9), glob4_OH(), glob3_OH(:,1), glob6_OH()];
Labels={'Worst_YHand', 'Worst_YHand_Extension', 'Worst_YHand_Flexion', 'Best_YHand',...
    'ALL_YHands', 'Left_YHand', 'Right_YHand', 'Extension', 'Flexion',...
    'Left_Extension', 'Left_Flex', 'Right_Extension', 'Right_Flex',...
    'Worst_Direction', 'Worst_YHand-Best_YHand', 'Best_Direction'};

for i=1:16
    OHnik=deNaNize(globSep_OH(:,i));    
    Hnik=deNaNize(globSep_YH(:,i));
    roc_nik(OHnik, Hnik, Labels{i});
end

% for i=1:16
%     Nnik=deNaNize(globSep_YH(:,i));
%     OHnik=deNaNize(globSep_OH(:,i));
%     maxn=max(Nnik);
%     minn=min(Nnik);
%     maxpk=max(OHnik);
%     minpk=min(OHnik);
%     maxf=max(maxn,maxpk);
%     minf=min(minn,minpk);
%     step=(maxf-minf)/1000;
%     edges(i,:)=[minf:step:maxf,inf];
%     nn=histc(Nnik,edges(i,:));
%     nn=nn/sum(nn)*100;
%     npk=histc(OHnik,edges(i,:));
%     npk=npk/sum(npk)*100;
%     
%     %xxx find best separator value
%     [temp len]=size(edges);
%     for j=1:len
%         tp=sum(npk(j:len)); %true positive
%         if j==1
%             fn=0;
%         else
%             fn=sum(npk(1:(j-1)));   %false negative
%         end
%         fp=sum(nn(j:len));  %false positive
%         if j==1
%             tn=0;
%         else            
%             tn=sum(nn(1:(j-1)));    %true negative
%         end
%         %lamda value must be maximum
% %         lamda=(tp*tn)/((fp*fn)+1);
%         lamda=tn;
% %         lamda=tp;
% 
%         edgeslamda(i,j)=lamda;
%     end
%     [temp2 ind]=max(edgeslamda(i,:));
%     
%     separator(i)=edges(i,ind);
%     sucfactor(i)=edgeslamda(i,ind)
% 
%    
%     finaltp(i)=sum(npk(ind:length(npk)));
%     finaltn(i)=sum(nn(1:ind-1));
%     finalfp(i)=sum(nn(ind:length(nn)));
%     finalfn(i)=sum(npk(1:ind-1));
%     
%     fprintf('Label=%s\n', Labels{i});
%     fprintf('TPOS=%f, TNEG=%f, FP=%f, FN=%f, SEPARATOR=%f\n',finaltp(i),finaltn(i),finalfp(i),finalfn(i),separator(i)); 
%    
% %%%%%%% Uncomment this to build Distribution Figure%%%%%%%%%%%%%
% %     figure(3);
% %     Nedges=length(edges)-1;
% %     hold off;
% %     xlabel('Mean Score (worst hand)');
% %     ylabel('Percentage of population');
% %     hold on;
% %     bar(edges(i,1:Nedges),[nn(1:Nedges) npk(1:Nedges)],'stack')
% %     legend('N','OH');
% %     colormap bone;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
%     
%     h=figure(4);
%     hold off;
%     plot(edges(i,:), npk);
%     title(Labels{i});
%     xlabel('Mean Values of index');
%     ylabel('Percentage of population');
%     hold on;
%     plot(edges(i,:),nn,'r');
%     legend('OH','N');
%     percent=[0 10 20 30 40 50 60 70 80];
%     value=ones(9)*separator(i);
%     plot(value,percent,'g');
%     annot1=strcat('<- point of distinction: ',num2str(separator(i)));
%     text(separator(i),40,annot1,'HorizontalAlignment','left');
%     annot2=strcat('success factor: ',num2str(sucfactor(i)));
%     text((separator(i)+0.0018),36,annot2,'HorizontalAlignment','left');
%     text((separator(i)+0.0018),32, strcat('TPOS=',num2str(finaltp(i)),' TNEG=',num2str(finaltn(i))), 'HorizontalAlignment','left');
%     text((separator(i)+0.0018),28, strcat('FP=',num2str(finalfp(i)),' FN=',num2str(finalfn(i))), 'HorizontalAlignment','left');
%     text((separator(i)+0.0018),24, strcat('SEPARATOR=',num2str(separator(i))), 'HorizontalAlignment','left');
%         
%     tag=Labels{i};
%     mkdir(strcat('../Separations/',folder));
%     saveas(h,strcat('../Separations/',folder,'/S_',tag),'jpg');
%  
% %     pause;
% 
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---THE END---%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
