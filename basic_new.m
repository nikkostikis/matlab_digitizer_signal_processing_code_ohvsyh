function [Qindex,duration,meanvel]=basic_new(fname, synth, index, sampling)
% The index input has the following values:
% NVV, MV, SDV, ETPv, ETPy

profile off;
raw_data=load(fname);
if size(raw_data,1)<2 %not enough datapoints - return
    Qindex=[];
    return;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=raw_data(:,2)/1000; %convert ms to sec
x=raw_data(:,3:4);

duration = t(end);

% scaling from pixels to cm
% 150 pixels from x already have been removed
% in the raw files
[cm_per_px res]=rescalc(x(:,1));
x(:,1)=x(:,1)*cm_per_px;
x(:,2)=x(:,2)*cm_per_px;

y=x(:,2);
x=x(:,1);

xWithoutRemovingSameTimestampPoints = x;
tWithoutRemovingSameTimestampPoints = t;
% check whether there are duplicates in the time vector, and discard
% should this happen? Do I trust the timer recording duplicae times or the
% displacements? Discarding the points gives a different path and these
% points are too many (half on average)

if (sum(diff(t)==0))>0 && strcmp(sampling, 'low')
    % idx=find(diff(t)~=0);
    % Correction of idx calculation in order to ommit same values after
    % first occurence (07MAY2018)
    idx = [1; find(diff(t)~=0) + 1];
    t=t(idx);
    x=x(idx,:);
    y=y(idx,:);
else
    idx = 1:length(x);
end

%%%%%%% Percentage Same Time %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
percentageSameTime = (1 - length(idx) / length(xWithoutRemovingSameTimestampPoints)) * 100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define dt in theory %%%%%%%%%%
Fs = 1 / (t(end) / length(x));
dt_theory = t(end) / length(x);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dt = dt_theory;
% dt = abs(diff(t));

% the mean velocity is in absolute value.
meanvel=abs(x(end)-x(1))/t(end);     
% per point difference in velocity.
dvel=abs(diff(x)) ./ dt;

if strcmp(index,'NVV') 
    % Old NVV, as described in BMP paper
    % The NVV can be seen as the sum of velocity differences to
    % the total displacement. That is cm/sec/cm. 
    % Ultimately it is 1/sec/1000 => 1/ms.
    Qindex = sum(abs(diff(dvel)))  /  t(end)  /  meanvel / 1000;

elseif strcmp(index,'MV')
    
    Qindex=meanvel;

elseif strcmp(index,'SDV')
    
    Qindex=std(dvel);

elseif strcmp(index,'ETPv')

    % to calculate the probability of occurence I assume equal dvels at 
    % 4 digits, build an Alphabet of 'symbols' and calculate the 
    % entropy in nikEntropy
    Qindex=nikEntropy(dvel',4);

elseif strcmp(index,'ETPy')

    % Avoid rounding to n-th decimal
    Qindex=nikEntropy(y); 

elseif strcmp(index,'ETPy4')

    Qindex=nikEntropy(y,4);

elseif strcmp(index,'ETPyRot')

    traj = yTrajectoryCorrection(x,y);
    % Avoid rounding to n-th decimal
    Qindex=nikEntropy(traj(:,2)'); 

elseif strcmp(index,'ETPyRot4')

    traj = yTrajectoryCorrection(x,y);
    Qindex=nikEntropy(traj(:,2)',4);
    
else
    error('Unknown metric to be calculated');
end

% mfile=strcat('Basic_Results_',synth,'_',index,'.txt');
% mfid=fopen(mfile,'a+');
% fprintf(mfid,'%s\t%d\t%f\t%f\t%f\n',fname, res, duration, Qindex, meanvel);
% fclose(mfid);

% mfile=strcat('Percentage_Same_Time_',synth,'.txt');
% mfid=fopen(mfile,'a+');
% fprintf(mfid,'%s\t%f\n',fname, percentageSameTime);
% fclose(mfid);