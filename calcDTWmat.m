function calcDTWmat 

% git comment

load 'ParsedData/signals_OH.mat'
load 'ParsedData/fileNames_OH.mat'

load 'ParsedData/signals_PD.mat'
load 'ParsedData/fileNames_PD.mat'

% Claculate DTW d's to a different mat for each signal
% The idea is to be able to use the mats before the execution ends

numPD = size(fileNames_PD); %count of PD volunteers +1 header
numPD = numPD(1) - 1; % -1 due to headers

numOH = size(fileNames_OH); %count of OH volunteers +1 header
numOH = numOH(1) - 1 ; % -1 due to headers

% logFile=strcat();
logFid=fopen('Progress_Log.txt','w+');

% numPD = 7; % Testing files
% numOH = 3; % Testing files

fprintf(logFid,'Entered PD DTW calculations\n'); % Initial entry to logFile
for i = 2 : numPD + 1
    s1 = signals_PD{i, 1};
    tempD = zeros(1,numPD);
    for j = 2 : numPD + 1
       s2 = signals_PD{j, 1};
       tempD(j-1) = dtw(s1, s2);
%        fprintf('PD signal no: %d \n', j-1);
    end
    % save tempD to an approriate variable
    eval(strcat('DTWmat_PD_fnum_', num2str(i-1), '= tempD;'));
    % save the variable to file
    save( strcat('DTWMATS/DTWmat_PD_fnum_', num2str(i-1), '.mat'), strcat('DTWmat_PD_fnum_', num2str(i-1)) );
    
    fprintf(logFid, 'Completed all for PD signal: %d \n', i-1);
end

fprintf(logFid,'\nEntered OH DTW calculations\n'); % Initial entry to logFile
for i = 2 : numOH + 1    
    s1 = signals_OH{i, 1};
    tempD = zeros(1,numOH);
    for j = 2 : numOH + 1
       s2 = signals_OH{j, 1};
       tempD(j-1) = dtw(s1, s2);
%        fprintf('H signal no: %d \n', j-1);
    end
    % save tempD to an approriate variable
    eval(strcat('DTWmat_OH_fnum_', num2str(i-1), '= tempD;'));
    % save the variable to file
    save( strcat('DTWMATS/DTWmat_OH_fnum_', num2str(i-1), '.mat'), strcat('DTWmat_OH_fnum_', num2str(i-1)) );
    
    fprintf(logFid, 'Completed all for OH signal: %d \n', i-1);
end

fclose(logFid);

% DTWmat = zeros(numPD + numOH, numPD + numOH);

% for i = 2 : numPD + 1 + numOH
%     s1 = combSignalsC{i, 1};
%     i;
%     tempD = zeros(1,numPD + numOH);
%     for j = 2 : numPD + 1 + numOH
%        s2 = combSignalsC{j, 1};
%        tempD(j-1) = dtw(s1, s2);
%        j;
%     end
%     DTWmat(i-1,:) = tempD;
%     fprintf('Completed subject: %d \n', i-1);
% end