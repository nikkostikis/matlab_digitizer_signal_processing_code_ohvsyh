% Called by Homoskdstty_Testr

function homosked(A,B,column)

alpha=0.01;
A=deNaNize(A);
B=deNaNize(B);
AA=[A ones(size(A))];
BB=[B 2*ones(size(B))];    
X=[AA;BB];
fprintf('Brown-Forsythe test for %s\n', column);
BFtest(X,alpha);

s=max(size(A),size(B));
Y=ones(s,2).*NaN;
Y(1:size(A),1)=A;
Y(1:size(B),2)=B;
fprintf('Levene test for %s\n', column);
p=vartestn(Y,[],'off','robust');
if p<alpha 
    fprintf('Levene says %s not homoscedastic\n', column);
else
    fprintf('Levene says %s homoscedastic\n', column);
end