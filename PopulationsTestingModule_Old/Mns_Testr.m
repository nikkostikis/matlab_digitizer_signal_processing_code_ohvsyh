FilesReference

fprintf('\nSTART%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

fprintf('\nMeans Equality for MEANS\n\n');

tstt(glob2_H(:,1), glob2_H(:,4), 'wh vs bh intra H','unequal');
tstt(glob2_H(:,1), glob2_H(:,4), 'wh vs bh intra H','equal');
mw(glob2_H(:,1), glob2_H(:,4), 'wh vs bh intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob2_H(:,1), globM_H(:,1), 'wh vs allh intra H','unequal');
tstt(glob2_H(:,1), globM_H(:,1), 'wh vs allh intra H','equal');
mw(glob2_H(:,1), globM_H(:,1), 'wh vs allh intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob4_H, globM_H(:,1), 'wd vs allh intra H','unequal');
tstt(glob4_H, globM_H(:,1), 'wd vs allh intra H','equal');
mw(glob4_H, globM_H(:,1), 'wd vs allh intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');


tstt(glob4_H, glob6_H, 'wd vs bd intra H','unequal');
tstt(glob4_H, glob6_H, 'wd vs bd intra H','equal');
mw(glob4_H, glob6_H, 'wd vs bd intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');


tstt(glob2_PD(:,1), glob2_PD(:,4), 'wh vs bh intra PD','unequal');
tstt(glob2_PD(:,1), glob2_PD(:,4), 'wh vs bh intra PD','equal');
mw(glob2_PD(:,1), glob2_PD(:,4), 'wh vs bh intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob2_PD(:,1), globM_PD(:,1), 'wh vs allh intra PD','unequal');
tstt(glob2_PD(:,1), globM_PD(:,1), 'wh vs allh intra PD','equal');
mw(glob2_PD(:,1), globM_PD(:,1), 'wh vs allh intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob4_PD, globM_PD(:,1), 'wd vs allh intra PD','unequal');
tstt(glob4_PD, globM_PD(:,1), 'wd vs allh intra PD','equal');
mw(glob4_PD, globM_PD(:,1), 'wd vs allh intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');


tstt(glob4_PD, glob6_PD, 'wd vs bd intra PD','unequal');
tstt(glob4_PD, glob6_PD, 'wd vs bd intra PD','equal');
mw(glob4_PD, glob6_PD, 'wd vs bd intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');


tstt(globM_H(:,1), globM_PD(:,1), 'allh inter','unequal');
tstt(globM_H(:,1), globM_PD(:,1), 'allh inter','equal');
mw(globM_H(:,1), globM_PD(:,1), 'allh inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob2_H(:,1), glob2_PD(:,1), 'wh inter','unequal');
tstt(glob2_H(:,1), glob2_PD(:,1), 'wh inter','equal');
mw(glob2_H(:,1), glob2_PD(:,1), 'wh inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob4_H, glob4_PD, 'wd inter','unequal');
tstt(glob4_H, glob4_PD, 'wd inter','equal');
mw(glob4_H, glob4_PD, 'wd inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(glob3_H(:,1), glob3_PD(:,1), 'wh-bh inter','unequal');
tstt(glob3_H(:,1), glob3_PD(:,1), 'wh-bh inter','equal');
mw(glob3_H(:,1), glob3_PD(:,1), 'wh-bh inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');


fprintf('\nMeans Equality for ALL\n\n');

tstt(globH_H{1}, globH_H{4}, 'wh vs bh intra H','unequal');
tstt(globH_H{1}, globH_H{4}, 'wh vs bh intra H','equal');
mw(globH_H{1}, globH_H{4}, 'wh vs bh intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(globH_H{1}, globA_H{9}, 'wh vs allh intra H','unequal');
tstt(globH_H{1}, globA_H{9}, 'wh vs allh intra H','equal');
mw(globH_H{1}, globA_H{9}, 'wh vs allh intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(globHD_H, globA_H{9}, 'wd vs allh intra H','unequal');
tstt(globHD_H, globA_H{9}, 'wd vs allh intra H','equal');
mw(globHD_H, globA_H{9}, 'wd vs allh intra H');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');


tstt(globH_PD{1}, globH_PD{4}, 'wh vs bh intra PD','unequal');
tstt(globH_PD{1}, globH_PD{4}, 'wh vs bh intra PD','equal');
mw(globH_PD{1}, globH_PD{4}, 'wh vs bh intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(globH_PD{1}, globA_PD{9}, 'wh vs allh intra PD','unequal');
tstt(globH_PD{1}, globA_PD{9}, 'wh vs allh intra PD','equal');
mw(globH_PD{1}, globA_PD{9}, 'wh vs allh intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(globHD_PD, globA_PD{9}, 'wd vs allh intra PD','unequal');
tstt(globHD_PD, globA_PD{9}, 'wd vs allh intra PD','equal');
mw(globHD_PD, globA_PD{9}, 'wd vs allh intra PD');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');


tstt(globA_H{9}, globA_PD{9}, 'allh inter','unequal');
tstt(globA_H{9}, globA_PD{9}, 'allh inter','equal');
mw(globA_H{9}, globA_PD{9}, 'allh inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(globH_H{1}, globH_PD{1}, 'wh inter','unequal');
tstt(globH_H{1}, globH_PD{1}, 'wh inter','equal');
mw(globH_H{1}, globH_PD{1}, 'wh inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

tstt(globHD_H, globHD_PD, 'wd inter','unequal');
tstt(globHD_H, globHD_PD, 'wd inter','equal');
mw(globHD_H, globHD_PD, 'wd inter');
fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

fprintf('\nEND%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');


