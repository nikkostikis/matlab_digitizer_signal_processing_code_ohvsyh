FilesReference
fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nNormality for MEANS\n\n');
[hJBallN hLillieallN]=normality(globM_H(:,1),'allh N');
[hJBwhN hLilliewhN]=normality(glob2_H(:,1),'wh N');
[hJBbhN hLilliebhN]=normality(glob2_H(:,4),'bh N');
[hJBwdN hLilliewdN]=normality(glob4_H,'wd N');
[hJBfN hLilliefN]=normality(globM_H(:,5),'F N');
[hJBeN hLillieeN]=normality(globM_H(:,4),'E N');
[hJBfN hLilliefN]=normality(globM_H(:,2),'L N');
[hJBeN hLillieeN]=normality(globM_H(:,3),'R N');
[hJBwhbhN hLilliewhbhN]=normality(glob3_H(:,1),'wh-bh N');


fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
[hJBallPD hLillieallPD]=normality(globM_PD(:,1),'allh PD');
[hJBwhPD hLilliewhPD]=normality(glob2_PD(:,1),'wh PD');
[hJBbhPD hLilliebhPD]=normality(glob2_PD(:,4),'bh PD');
[hJBwdPD hLilliewdPD]=normality(glob4_PD,'wd PD');
[hJBfPD hLilliefPD]=normality(globM_PD(:,5),'f PD');
[hJBePD hLillieePD]=normality(globM_PD(:,4),'e PD');
[hJBlPD hLillilPD]=normality(globM_PD(:,2),'L PD');
[hJBrPD hLillierPD]=normality(globM_PD(:,3),'R PD');
[hJBwhbhPD hLilliewhbhPD]=normality(glob3_PD(:,1),'wh-bh PD');

fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nNormality for ALL\n\n');
[hJBallN hLillieallN]=normality(globA_H{9},'allh N');
[hJBwhN hLilliewhN]=normality(globH_H{1},'wh N');
[hJBbhN hLilliebhN]=normality(globH_H{4},'bh N');
[hJBwdN hLilliewdN]=normality(globHD_H,'wd N');
[hJBfN hLilliefN]=normality(globA_H{4},'f N');
[hJBeN hLillieeN]=normality(globA_H{3},'e N');
[hJBlN hLillielN]=normality(globA_H{1},'L N');
[hJBrN hLillierN]=normality(globA_H{2},'R N');

fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
[hJBallPD hLillieallPD]=normality(globA_PD{9},'allh PD');
[hJBwhPD hLilliewhPD]=normality(globH_PD{1},'wh PD');
[hJBbhPD hLilliebhPD]=normality(globH_PD{4},'bh PD');
[hJBwdPD hLilliewdPD]=normality(globHD_PD,'wd PD');
[hJBfPD hLilliefPD]=normality(globA_PD{4},'f PD');
[hJBePD hLillieePD]=normality(globA_PD{3},'e PD');
[hJBlPD hLillielPD]=normality(globA_PD{1},'L PD');
[hJBrPD hLillierPD]=normality(globA_PD{2},'R PD');

