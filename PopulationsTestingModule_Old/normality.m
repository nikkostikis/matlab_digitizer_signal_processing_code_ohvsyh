% Called by Normlty_Testr

function [hJB pJB hLillie pLillie]=normality(X,column)
X=deNaNize(X);
% fprintf('\nJarque Berra test: h=1 if data is NOT normal\n');
if max(size(X))<4
    hJB=1000;
    hLillie=1000;
    
    fprintf('Unable to perform normality test-Lack of observations\n\n');
else

    [hJB, pJB]=jbtest(X,0.01);
    if hJB==1
        fprintf('JB says %s not normal\n\n', column);
        
    else
        fprintf('JB says %s normal\n\n', column);
        
    end 
    pJB;
%     fprintf('\nLilliefors test: h=1 if data is NOT normal\n');
    [hLillie, pLillie] = lillietest(X,0.01);
    if hLillie==1
        fprintf('Lillie says %s not normal\n\n', column);
        
    else
        fprintf('Lillie says %s normal\n\n', column);
        
    end
    pLillie;
    fprintf('\n\n');
end