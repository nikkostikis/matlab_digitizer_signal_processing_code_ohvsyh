function runner_Testr()

FilesReference

fprintf('Mean Median and Variance for each H subject --> MeanMedVar(''H'') Running\n\n');
MeanMedVar(allData_H,'H');
% pause;
% clc;

fprintf('Mean Median and Variance for each H subject --> MeanMedVar(''PD'') Running\n\n');
MeanMedVar(allData_PD,'PD');
% pause;
% clc;

fprintf('Intra Normality --> IntraNormality Running\n\n');
IntraNormality;
% pause;
% clc;

fprintf('\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n\nIntra Means for Left vs Right hand--> NormMeansEachOne Running\n\n');
NormMeansEachOne;
% pause;
% clc;

fprintf('\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n\nMeanValue and St_Dev for each one --> MeanStd_Testr Running\n\n');
MeanStd_Testr; %1
% pause;
% clc;

fprintf('\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n\nTesting Normality --> Normlty_Testr Running \n\n');
Normlty_Testr; %2
% pause;
% clc;

fprintf('\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n\nTesting Homoskedastisitty --> Homoskdststty_Testr Running \n\n');
Homoskdstty_Testr; %3
% pause;
% clc;

fprintf('\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n!!!!!!!!!!!!\n\nTesting Means of populations --> Mns_Testr Running \n\n');
Mns_Testr; %4
% clc;

