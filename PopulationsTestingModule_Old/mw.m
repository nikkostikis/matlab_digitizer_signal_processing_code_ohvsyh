% Called by Mns_Testr
function mw(A,B, column)


A=deNaNize(A);
B=deNaNize(B);

fprintf('\nMann-Whitney test: h=1 if means are NOT the same\n');

[pmw,hMW] = ranksum(A,B,'alpha',0.01)
if hMW==1
    fprintf('MW says %s means not equal\n',column);
else
    fprintf('MW says %s means equal\n', column);
end  
pmw
fprintf('end of Mann-Whitney test/n/n');


% fprintf('\n2-sample Kolmogorov-Smirnov test: h=1 if data not from same distribution\n');
% hKS = kstest2(A,B)
% if hKS==1
%     fprintf('KS says %s distributions not same\n', column);
% else
%     fprintf('KS says %s distributions same\n', column);
% end  

