function [c res]=rescalc(x) %x=x vector

ref=max(x); %either start or finish of movement

if (ref<=1024)
    res=1024;
elseif (ref<=1280)
    res=1280;
elseif (ref<=1366)
    res=1366;
elseif (ref<=1440)
    res=1440;
elseif (ref<=1680)
    res=1680;
elseif (ref<=1920)
    res=1920;
end

padused=14.7*(1-(300/res)); %cm of pad used
lcdused=res-300; %px of screen used
c=padused/lcdused;