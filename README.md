# README #

This repository does not contain fully runnable code. The data needed are -not present neither online- EDIT: are now in the ParsedData folder. The code uses files which are not online but with a few tweaks the code can be ran with the ParsedData mats. 

The repo's purpose is mainly archiving and backup.

### What is this repository for? ###

The code performs digital processing of signals collected using a WACOM digitizer. PD patients and healthy volunteers were asked to draw simple straight lines with both their hands, extending and flexing the elbow. We extract features (metrics) and perform statistical analyses on the signals/metrics.