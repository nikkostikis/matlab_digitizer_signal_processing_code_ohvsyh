load 'DTWmat_PD_1_161.mat';
d = DTWmat_PD(1:161, :);
indzero = find(d==0); %find zeros and get linear indices
d([indzero]) = inf;
minperrow = zeros(161,1);
indminperrow = zeros(161,1);
for i = 1:161
    row = d(i,:);
    minperrow(i) = min(row);
    indminperrow(i) = find(row == minperrow(i));
end
% I need a histogram with 1068 bins to see how the 161 signals are matching
% (min(d)) with every other
h = hist(indminperrow, 1068);