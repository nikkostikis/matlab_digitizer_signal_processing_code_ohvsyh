% Choose the metric through comments in the basic_new
% Choose the axis/axes here
% Create folders ProcessedData, ROCs and ROCs_Figs
% Run
clear all;
clc;

index = 'NVV';
% index = 'MV';
% index = 'SDV';
% index = 'ETPv';
% index = 'ETPy';
% index = 'ETPy4';
% index = 'ETPyRot';
% index = 'ETPyRot4';

sampling = 'high'; %i.e. 133Hz
% sampling = 'low'; %i.e. 60Hz

% Prepare txt file for all PD
% mfile=strcat('Basic_Results_PD','_',index,'.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tResolution\tDuration\tQindex\tMeanVel\n');
% fclose(mfid);
% 
% mfile=strcat('Percentage_Same_Time_PD.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tPercentageSameTime\n');
% fclose(mfid);
batch_new('OH','ALL',index,sampling)
batch_new('OH','MEANS',index,sampling)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prepare txt file for all H
% mfile=strcat('Basic_Results_H','_',index,'.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tResolution\tDuration\tQindex\tMeanVel\n');
% fclose(mfid);
% 
% mfile=strcat('Percentage_Same_Time_H.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tPercentageSameTime\n');
% fclose(mfid);
batch_new('YH','ALL',index,sampling)
batch_new('YH','MEANS',index,sampling)

% It creates differences (Worst-Best) and basic comparisons
% STATPROC
STATPROC_MEANS