function tstt(A,B,column,vars)
A=deNaNize(A);
B=deNaNize(B);
fprintf('2-sample T-test: h=1 if means are NOT the same --%s\n', vars);
[hTT,ptt] = ttest2(A,B,0.01,[],vars)
if hTT==1
    fprintf('TT says %s means not equal\n', column);
else
    fprintf('TT says %s means equal\n', column);
end 
ptt
fprintf('End of 2-sample T-test --%s\n\n', vars)
