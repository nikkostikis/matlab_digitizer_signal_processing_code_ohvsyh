% calculates all metrics given a processed (scaled) x or xy signal and its
% time.
% called by basic_new
% the signal should be processed similarly to the signals2cells output.
% calls entropy.m

function [NVV ETP meanVel SDV] = calcMetrics(x, t)

meanVel = (x(length(t))-x(1))/(t(length(t))-t(1));
qvel = diff(x)./ diff(t);
NVV = sum(abs(diff(qvel)))/(t(length(t))-t(1))/abs(meanVel)/1000;

SDV = std(qvel);

ETP = entropy(qvel');