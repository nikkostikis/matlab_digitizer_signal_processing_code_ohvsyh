% Called to save signals in cells and mat
% in: PDH label
% out: 
% Creates a cell with the full name of the file, the person's code,
% the move code, the hand code and the class (cell: 5 columns)
% Creates a cell with all the signals, containing the signals x and y, 
% the time, the duration and the MeanVel (cell: 5 columns)

function signals2cells(PDH)
close all;
clc;

%Comment out according to folder/populations
if (strcmp(PDH,'PD')==1)
    dirname='../../Data/PD'; synth='PD';
elseif (strcmp(PDH,'OH')==1)
    dirname='../../Data/OH'; synth='OH';
end

D=dir(dirname);
D=D(3:length(D));

fileNamesC = {};
fileNamesC(1, 1:5) = {'personCode', 'fileName', 'moveCode', 'handCode', 'class'};

signalsC = {};
signalsC(1, 1:5) = {'signalx', 'signaly', 'time', 'duration', 'meanVelocity'};

fcount=0;
for j=1:length(D);
    j;
    fname=D(j).name

    if ~strcmp(fname,'.DS_Store')
        fcount = fcount + 1
        % extract labels from file name
        idx=find(fname=='_'); % find underscore indices
        fileNamesC{end+1, 1} = fname(1 : idx(1)-1); % personCode
        fileNamesC{end, 2} = fname; % fileName - end is now incremented from previous command so you only need <end>
        % There is a problem with the file names
        % The labels for move and hand are swapped in positions within file name 
        % for newer files (>=286).
        % To avoid depending on file count I build a string with both labels 
        % and use strfind to define appropriate moveCode and handCode
        moveHandLabels = strcat(fname(idx(2) + 1 ), fname(idx(3) + 1 ));
        % Define moveCode (E,F)
        if strfind(moveHandLabels, 'E')
            moveCode = 'E';
        else
            moveCode = 'F';
        end
        % Define handCode (R,L)
        if strfind(moveHandLabels, 'R')
            handCode = 'R';
        else
            handCode = 'L';
        end
        fileNamesC{end, 3} = moveCode;
        fileNamesC{end, 4} = handCode;
             
        fileNamesC{end, 5} = synth; % class
  
        longfname=strcat(dirname,'/',fname);
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            raw_data=load(longfname);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            t=raw_data(:,2)/1000; %convert ms to sec
            x=raw_data(:,3:4);

            % scaling from pixels to cm - Do not remove 150 from x, it is
            % already removed
            cm_per_px=rescalc(x(:,1));
            x(:,1)=x(:,1)*cm_per_px;
            x(:,2)=x(:,2)*cm_per_px;
            
            y=x(:,2);            
            x=x(:,1);
            
            % check whether there are duplicates in the time vector, and discard
%             if (sum(diff(t)==0))>0
%                 idx=find(diff(t)~=0);
%                 t=t(idx);
%                 x=x(idx,:);
%             end

            % meanvel is cm/sec
            meanvel=abs(x(end)-x(1))/(t(end));
            % duration in sec
            duration=t(end);
  
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Only the first should be placed at end+1,
        % this increases end, so end must be used for the
        % following assignments
        signalsC{end+1, 1} = x;
        signalsC{end, 2} = y;
        signalsC{end, 3} = t;
        signalsC{end, 4} = duration;
        signalsC{end, 5} = meanvel;
    end
end
fclose all;
fileNamesfile=strcat('ParsedData/fileNames_',synth,'.mat');
signalsfile=strcat('ParsedData/signals_',synth,'.mat');

fN=strcat('fileNames_',synth);
eval(strcat(fN,'=fileNamesC;'));
eval(['save',' ',fileNamesfile,' ',fN]);

s=strcat('signals_',synth);
eval(strcat(s,'=signalsC;'));
eval(['save',' ',signalsfile,' ',s]);


keyboard; % like a pause kind of thing
return;