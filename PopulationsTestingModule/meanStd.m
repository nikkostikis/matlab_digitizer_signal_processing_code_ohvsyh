% Called by MeanStd_Testrt

function [meanval stdval]=meanStd(A, column)


A=deNaNize(A);


meanval=mean(A);
stdval=std(A);

fprintf('Mean value for %s is %f\n', column, meanval);
fprintf('St Deviation for %s is %f\n', column, stdval);
