% Called by Homoskdstty_Testr

function homosked(A,B,column)

alpha=0.01;
A=deNaNize(A);
B=deNaNize(B);

% AA=[A ones(size(A))];
% BB=[B 2*ones(size(B))];    
% X=[AA;BB];
% fprintf('Brown-Forsythe test for %s\n', column);
% BFtest(X,alpha);

Y = [A; B];

s = numel(A) + numel(B);
% Y=ones(s,2).*NaN;
% Y(1:size(A),1)=A;
% Y(1:size(B),2)=B;

groups = cell(s, 1);
groups(1:numel(A)) = {strcat(column,'_H')};
groups(numel(A)+1:s) = {strcat(column,'_PD')};

% fprintf('Brown-Forsythe test for %s\n', column);
p=vartestn(Y, groups,'TestType','BrownForsythe', 'Display', 'on');
if p<alpha 
    fprintf('Brown-Forsythe says %s not homoscedastic\n', column);
else
    fprintf('Brown-Forsythe says %s homoscedastic\n', column);
end
saveas(figure(1), strcat('Homosked_Figures/BrownForsythe/Data_',column));
saveas(figure(2), strcat('Homosked_Figures/BrownForsythe/BoxPlots_',column));
close all;



% fprintf('Levene test for %s\n', column);
p=vartestn(Y,groups,'TestType','LeveneAbsolute', 'Display', 'on')
if p<alpha 
    fprintf('Levene says %s not homoscedastic\n', column);
else
    fprintf('Levene says %s homoscedastic\n', column);
end
saveas(figure(1), strcat('Homosked_Figures/Levene/Data_',column));
saveas(figure(2), strcat('Homosked_Figures/Levene/BoxPlots_',column));
close all;
