% Called by runner_Testr
function MeanMedVar(data,PDN)

% d=strcat('allData_',PDN);
% allData=eval(d);
allData=data;
pcount=max(size(allData));
for i=1:pcount
    fprintf ('Mean for subject %d \n',i);
    res1(i)=mean(allData{i,10});
    fprintf ('Median for subject %d \n',i);
    res2(i)=median(allData{i,10});
    fprintf ('Variance for subject %d \n',i);
    res3(i)=var(allData{i,10});
end
fprintf ('Mean values for %s',PDN);
res1

fprintf ('Median values for %s',PDN);
res2

fprintf ('Variance values for %s',PDN);
res3
