FilesReference
MeanValues=zeros(32,1);
Stds=zeros(32,1);

fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');

fprintf('\nMeanStd for MEANS\n\n');
[MeanValues(1,1) Stds(1,1)]=meanStd(globM_YH(:,1),'allh YH');
[MeanValues(2,1) Stds(2,1)]=meanStd(glob2_YH(:,1),'wh YH');
[MeanValues(3,1) Stds(3,1)]=meanStd(glob2_YH(:,4),'bh YH');
[MeanValues(4,1) Stds(4,1)]=meanStd(glob4_YH,'wd YH');
[MeanValues(5,1) Stds(5,1)]=meanStd(globM_YH(:,5),'F YH');
[MeanValues(6,1) Stds(6,1)]=meanStd(globM_YH(:,4),'E YH');
[MeanValues(7,1) Stds(7,1)]=meanStd(globM_YH(:,2),'L YH');
[MeanValues(8,1) Stds(8,1)]=meanStd(globM_YH(:,3),'R YH');

fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
[MeanValues(9,1) Stds(9,1)]=meanStd(globM_OH(:,1),'allh OH');
[MeanValues(10,1) Stds(10,1)]=meanStd(glob2_OH(:,1),'wh OH');
[MeanValues(11,1) Stds(11,1)]=meanStd(glob2_OH(:,4),'bh OH');
[MeanValues(12,1) Stds(12,1)]=meanStd(glob4_OH,'wd OH');
[MeanValues(13,1) Stds(13,1)]=meanStd(globM_OH(:,5),'f OH');
[MeanValues(14,1) Stds(14,1)]=meanStd(globM_OH(:,4),'e OH');
[MeanValues(15,1) Stds(15,1)]=meanStd(globM_OH(:,2),'L OH');
[MeanValues(16,1) Stds(16,1)]=meanStd(globM_OH(:,3),'R OH');


fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nMeanStd for ALL\n\n');
[MeanValues(17,1) Stds(17,1)]=meanStd(globA_YH{9},'allh YH');
[MeanValues(18,1) Stds(18,1)]=meanStd(globH_YH{1},'wh YH');
[MeanValues(19,1) Stds(19,1)]=meanStd(globH_YH{4},'bh YH');
[MeanValues(20,1) Stds(20,1)]=meanStd(globHD_YH,'wd YH');
[MeanValues(21,1) Stds(21,1)]=meanStd(globA_YH{4},'f YH');
[MeanValues(22,1) Stds(22,1)]=meanStd(globA_YH{3},'e YH');
[MeanValues(23,1) Stds(23,1)]=meanStd(globA_YH{1},'L YH');
[MeanValues(24,1) Stds(24,1)]=meanStd(globA_YH{2},'R YH');

fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
[MeanValues(25,1) Stds(25,1)]=meanStd(globA_OH{9},'allh OH');
[MeanValues(26,1) Stds(26,1)]=meanStd(globH_OH{1},'wh OH');
[MeanValues(27,1) Stds(27,1)]=meanStd(globH_OH{4},'bh OH');
[MeanValues(28,1) Stds(28,1)]=meanStd(globHD_OH,'wd OH');
[MeanValues(29,1) Stds(29,1)]=meanStd(globA_OH{4},'f OH');
[MeanValues(30,1) Stds(30,1)]=meanStd(globA_OH{3},'e OH');
[MeanValues(31,1) Stds(31,1)]=meanStd(globA_OH{1},'L OH');
[MeanValues(32,1) Stds(32,1)]=meanStd(globA_OH{2},'R OH');

[MeanValues(33,1) Stds(33,1)]=meanStd(glob3_YH(:,1),'wh-bh YH');
[MeanValues(34,1) Stds(34,1)]=meanStd(glob3_OH(:,1),'wh-bh OH');
