% Called by Mns_Testr

function tstt(A,B,column)
A=deNaNize(A);
B=deNaNize(B);

%%%% 2-sample T-test with equal variances
fprintf('2-sample T-test: h=1 if means are NOT the same EQUAL\n');
[hTT,ptt] = ttest2(A,B,0.01,[],'equal')
if hTT==1
    fprintf('T-Test says %s means not equal\n', column);
else
    fprintf('T-Test says %s means equal\n', column);
end 
ptt
fprintf('End of 2-sample T-test EQUAL\n\n')

%%%% 2-sample T-test with unequal variances
fprintf('2-sample T-test: h=1 if means are NOT the same --UNEQUAL\n');
[hTT,ptt] = ttest2(A,B,0.01,[],'unequal')
if hTT==1
    fprintf('T-Test says %s means not equal\n', column);
else
    fprintf('T-Test says %s means equal\n', column);
end 
ptt
fprintf('End of 2-sample T-test --UNEQUAL\n\n')

%%%% Mann-Whitney test 
fprintf('\nMann-Whitney test: h=1 if distributions are NOT the same\n');

[pmw,hMW] = ranksum(A,B,'alpha',0.01)
if hMW==1
    fprintf('Mann-Whitney says %s distributions not equal\n',column);
else
    fprintf('Mann-Whitney says %s distributions equal\n', column);
end  
pmw
fprintf('end of Mann-Whitney test/n/n');
