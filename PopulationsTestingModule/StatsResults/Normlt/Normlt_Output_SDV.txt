
Start Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 YH SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Normality for MEANS ALL YH

JB says ALL YH normal


pJB =

    0.1160

Lillie says ALL YH normal


pLillie =

    0.1427



Mean value for ALL YH is 1.410350
St Deviation for ALL YH is 0.323938


%%%%%%%%%%%%
Normality for MEANS LH YHealthy

JB says LH YH normal


pJB =

    0.2137

Lillie says LH YH normal


pLillie =

    0.0102



Mean value for LH YH is 1.331789
St Deviation for LH YH is 0.305338


%%%%%%%%%%%%
Normality for MEANS HH YH

JB says HH YH normal


pJB =

    0.1019

Lillie says HH YH normal


pLillie =

    0.0833



Mean value for HH YH is 1.499752
St Deviation for HH YH is 0.352540


%%%%%%%%%%%%
Normality for MEANS HLH YH

JB says HLH YH normal


pJB =

    0.1839

Lillie says HLH YH normal


pLillie =

    0.1598



Mean value for HLH YH is 0.167963
St Deviation for HLH YH is 0.128569


%%%%%%%%%%%%
Normality for MEANS LD YHealthy

JB says LD YH normal


pJB =

    0.3967

Lillie says LD YH normal


pLillie =

    0.0555



Mean value for LD YH is 1.271514
St Deviation for LD YH is 0.312063


%%%%%%%%%%%%
Normality for MEANS HD YH

JB says HD YH normal


pJB =

    0.1087

Lillie says HD YH normal


pLillie =

    0.1071



Mean value for HD YH is 1.602422
St Deviation for HD YH is 0.371384


%%%%%%%%%%%%
Normality for MEANS Left YH

JB says Left YH normal


pJB =

    0.1501

Lillie says Left YH normal


pLillie =

    0.0314



Mean value for Left YH is 1.386372
St Deviation for Left YH is 0.282083


%%%%%%%%%%%%
Normality for MEANS Right YH

JB says Right YH normal


pJB =

    0.1780

Lillie says Right YH normal


pLillie =

    0.0663



Mean value for Right YH is 1.445169
St Deviation for Right YH is 0.388852


%%%%%%%%%%%%
 OH SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Normality for MEANS ALL OH

JB says ALL OH normal


pJB =

    0.2307

Lillie says ALL OH normal


pLillie =

    0.1555



Mean value for ALL OH is 2.864920
St Deviation for ALL OH is 1.295732


%%%%%%%%%%%%
Normality for MEANS LH OH

JB says LH OH normal


pJB =

    0.1506

Lillie says LH OH normal


pLillie =

    0.2954



Mean value for LH OH is 2.634626
St Deviation for LH OH is 1.220525


%%%%%%%%%%%%
Normality for MEANS HH OH

JB says HH OH normal


pJB =

    0.3406

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('lillietest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m', 203)" style="font-weight:bold">lillietest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m',203,0)">line 203</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 23)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',23,0)">line 23</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 72)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',72,0)">line 72</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 26)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',26,0)">line 26</a>)] 
Lillie says HH OH normal


pLillie =

    0.5000



Mean value for HH OH is 3.087988
St Deviation for HH OH is 1.380638


%%%%%%%%%%%%
Normality for MEANS HLH OH

JB says HLH OH normal


pJB =

    0.2292

Lillie says HLH OH normal


pLillie =

    0.0160



Mean value for HLH OH is 0.453363
St Deviation for HLH OH is 0.304485


%%%%%%%%%%%%
Normality for MEANS LD OH

JB says LD OH normal


pJB =

    0.1114

Lillie says LD OH normal


pLillie =

    0.0292



Mean value for LD OH is 2.415759
St Deviation for LD OH is 1.135818


%%%%%%%%%%%%
Normality for MEANS HD OH

JB says HD OH normal


pJB =

    0.3145

Lillie says HD OH normal


pLillie =

    0.2147



Mean value for HD OH is 3.331564
St Deviation for HD OH is 1.466879


%%%%%%%%%%%%
Normality for MEANS Left OH

JB says Left OH normal


pJB =

    0.3146

Lillie says Left OH normal


pLillie =

    0.4875



Mean value for Left OH is 2.905667
St Deviation for Left OH is 1.384469


%%%%%%%%%%%%
Normality for MEANS Right OH

JB says Right OH normal


pJB =

    0.1439

Lillie says Right OH normal


pLillie =

    0.1110



Mean value for Right OH is 2.816947
St Deviation for Right OH is 1.258222


%%%%%%%%%%%%
End Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




OVER AND OUT!!

