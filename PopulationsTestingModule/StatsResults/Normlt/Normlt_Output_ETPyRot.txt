
Start Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 YH SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Normality for MEANS ALL YH

JB says ALL YH normal


pJB =

    0.1719

Lillie says ALL YH normal


pLillie =

    0.2669



Mean value for ALL YH is 7.134417
St Deviation for ALL YH is 0.330852


%%%%%%%%%%%%
Normality for MEANS LH YHealthy

JB says LH YH normal


pJB =

    0.1878

Lillie says LH YH normal


pLillie =

    0.1313



Mean value for LH YH is 7.054738
St Deviation for LH YH is 0.345585


%%%%%%%%%%%%
Normality for MEANS HH YH

JB says HH YH normal


pJB =

    0.2695

Lillie says HH YH normal


pLillie =

    0.2081



Mean value for HH YH is 7.222609
St Deviation for HH YH is 0.343476


%%%%%%%%%%%%
Normality for MEANS HLH YH

JB says HLH YH normal


pJB =

    0.0106

Lillie says HLH YH normal


pLillie =

    0.0563



Mean value for HLH YH is 0.167871
St Deviation for HLH YH is 0.141234


%%%%%%%%%%%%
Normality for MEANS LD YHealthy

JB says LD YH normal


pJB =

    0.1421

Lillie says LD YH normal


pLillie =

    0.0566



Mean value for LD YH is 6.947268
St Deviation for LD YH is 0.376733


%%%%%%%%%%%%
Normality for MEANS HD YH

JB says HD YH normal


pJB =

    0.2753

Lillie says HD YH normal


pLillie =

    0.1972



Mean value for HD YH is 7.335635
St Deviation for HD YH is 0.382909


%%%%%%%%%%%%
Normality for MEANS Left YH

JB says Left YH normal


pJB =

    0.3138

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('lillietest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m', 203)" style="font-weight:bold">lillietest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m',203,0)">line 203</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 23)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',23,0)">line 23</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 51)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',51,0)">line 51</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 118)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',118,0)">line 118</a>)] 
Lillie says Left YH normal


pLillie =

    0.5000



Mean value for Left YH is 7.154392
St Deviation for Left YH is 0.310940


%%%%%%%%%%%%
Normality for MEANS Right YH

JB says Right YH normal


pJB =

    0.3685

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('lillietest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m', 203)" style="font-weight:bold">lillietest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m',203,0)">line 203</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 23)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',23,0)">line 23</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 56)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',56,0)">line 56</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 118)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',118,0)">line 118</a>)] 
Lillie says Right YH normal


pLillie =

    0.5000



Mean value for Right YH is 7.122954
St Deviation for Right YH is 0.394066


%%%%%%%%%%%%
 OH SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Normality for MEANS ALL OH

JB says ALL OH normal


pJB =

    0.3832

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('lillietest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m', 203)" style="font-weight:bold">lillietest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m',203,0)">line 203</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 23)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',23,0)">line 23</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 62)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',62,0)">line 62</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 118)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',118,0)">line 118</a>)] 
Lillie says ALL OH normal


pLillie =

    0.5000



Mean value for ALL OH is 6.104395
St Deviation for ALL OH is 0.871384


%%%%%%%%%%%%
Normality for MEANS LH OH

JB says LH OH normal


pJB =

    0.4043

Lillie says LH OH normal


pLillie =

    0.4960



Mean value for LH OH is 5.971525
St Deviation for LH OH is 0.873262


%%%%%%%%%%%%
Normality for MEANS HH OH

JB says HH OH normal


pJB =

    0.3881

Lillie says HH OH normal


pLillie =

    0.2453



Mean value for HH OH is 6.243044
St Deviation for HH OH is 0.877793


%%%%%%%%%%%%
Normality for MEANS HLH OH

JB says HLH OH normal


pJB =

    0.1224

Lillie says HLH OH normal


pLillie =

    0.0253



Mean value for HLH OH is 0.271518
St Deviation for HLH OH is 0.203034


%%%%%%%%%%%%
Normality for MEANS LD OH

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('jbtest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/jbtest.m', 133)" style="font-weight:bold">jbtest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/jbtest.m',133,0)">line 133</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 13)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',13,0)">line 13</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 82)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',82,0)">line 82</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 118)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',118,0)">line 118</a>)] 
JB says LD OH normal


pJB =

    0.5000

Lillie says LD OH normal


pLillie =

    0.3628



Mean value for LD OH is 5.841075
St Deviation for LD OH is 0.823737


%%%%%%%%%%%%
Normality for MEANS HD OH

JB says HD OH normal


pJB =

    0.3421

Lillie says HD OH normal


pLillie =

    0.1887



Mean value for HD OH is 6.354391
St Deviation for HD OH is 0.859349


%%%%%%%%%%%%
Normality for MEANS Left OH

JB says Left OH normal


pJB =

    0.2030

Lillie says Left OH normal


pLillie =

    0.1280



Mean value for Left OH is 6.108042
St Deviation for Left OH is 0.960634


%%%%%%%%%%%%
Normality for MEANS Right OH

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('jbtest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/jbtest.m', 133)" style="font-weight:bold">jbtest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/jbtest.m',133,0)">line 133</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 13)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',13,0)">line 13</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 97)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',97,0)">line 97</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 118)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',118,0)">line 118</a>)] 
JB says Right OH normal


pJB =

    0.5000

[Warning: P is greater than the largest tabulated value, returning 0.5.] 
[> In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('lillietest', '/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m', 203)" style="font-weight:bold">lillietest</a> (<a href="matlab: opentoline('/Applications/MATLAB_R2015b.app/toolbox/stats/stats/lillietest.m',203,0)">line 203</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('normality', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m', 23)" style="font-weight:bold">normality</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/normality.m',23,0)">line 23</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Normlty_Testr', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m', 97)" style="font-weight:bold">Normlty_Testr</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Normlty_Testr.m',97,0)">line 97</a>)
  In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Save_Stats_2_File', '/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m', 118)" style="font-weight:bold">Save_Stats_2_File</a> (<a href="matlab: opentoline('/Users/nikmac/Desktop/Thesis/Digitizer_gig/Experiments folder/Main_Code/YHvsOH_deletedComments/PopulationsTestingModule/Save_Stats_2_File.m',118,0)">line 118</a>)] 
Lillie says Right OH normal


pLillie =

    0.5000



Mean value for Right OH is 6.106527
St Deviation for Right OH is 0.805660


%%%%%%%%%%%%
End Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




OVER AND OUT!!

