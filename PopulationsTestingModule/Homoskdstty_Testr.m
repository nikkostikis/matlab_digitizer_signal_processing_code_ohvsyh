% FilesReference
% MEAN values:
% ALL -> globM_XX(:,1)
% LH -> glob2_XX(:,4)
% HH -> glob2_XX(:,1)
% HLH -> glob3_XX(:,1)
% LD -> glob6_XX
% HD -> glob4_XX
% Left -> globM_XX(:,2)
% Right -> globM_XX(:,3)

fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n\n');
fprintf('\nHomoscedasticity for MEANS\n');
homosked(globM_YH(:,1), globM_OH(:,1), 'ALL');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob2_YH(:,4), glob2_OH(:,4), 'LH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob2_YH(:,1), glob2_OH(:,1), 'HH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob3_YH(:,1), glob3_OH(:,1), 'HLH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob6_YH, glob6_OH, 'LD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob4_YH, glob4_OH, 'HD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

homosked(globM_YH(:,2), globM_YH(:,3), 'Left vs Right YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(globM_OH(:,2), globM_OH(:,3), 'Left vs Right OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

homosked(glob2_YH(:,4), glob2_YH(:,1), 'LH vs HH YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob2_OH(:,4), glob2_OH(:,1), 'LH vs HH OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

