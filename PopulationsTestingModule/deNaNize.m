function A=deNaNize(A)

indA=find(~isnan(A));
A=A(indA);