% FilesReference
% MEAN values:
% ALL -> globM_XX(:,1)
% LH -> glob2_XX(:,4)
% HH -> glob2_XX(:,1)
% HLH -> glob3_XX(:,1)
% LD -> glob6_XX
% HD -> glob4_XX
% Left -> globM_XX(:,2)
% Right -> globM_XX(:,3)

MeanYH=zeros(8,1);
StdYH=zeros(8,1);

MeanOH=zeros(8,1);
StdOH=zeros(8,1);

fprintf('\nStart Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\n YH SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nNormality for MEANS ALL YH\n\n');
[hJBallYH, hLillieallYH]=normality(globM_YH(:,1),'ALL YH');
[MeanYH(1,1), StdYH(1,1)]=meanStd(globM_YH(:,1),'ALL YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LH YHealthy\n\n');
[hJBlhYH, hLillielhYH]=normality(glob2_YH(:,4),'LH YH');
[MeanYH(2,1), StdYH(2,1)]=meanStd(glob2_YH(:,4),'LH YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HH YH\n\n');
[hJBhhYH, hLilliehhYH]=normality(glob2_YH(:,1),'HH YH');
[MeanYH(3,1), StdYH(3,1)]=meanStd(glob2_YH(:,1),'HH YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HLH YH\n\n');
[hJBwhlhYH, hLilliewhlhYH]=normality(glob3_YH(:,1),'HLH YH');
[MeanYH(4,1), StdYH(4,1)]=meanStd(glob3_YH(:,1),'HLH YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LD YHealthy\n\n');
[hJBldYH, hLillieldYH]=normality(glob6_YH,'LD YH');
[MeanYH(5,1), StdYH(5,1)]=meanStd(glob6_YH,'LD YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HD YH\n\n');
[hJBhdYH, hLilliehdYH]=normality(glob4_YH,'HD YH');
[MeanYH(6,1), StdYH(6,1)]=meanStd(glob4_YH,'HD YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Left YH\n\n');
[hJBLeftYH, hLillieLeftYH]=normality(globM_YH(:,2),'Left YH');
[MeanYH(7,1), StdYH(7,1)]=meanStd(globM_YH(:,2),'Left YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Right YH\n\n');
[hJBRightYH, hLillieRightYH]=normality(globM_YH(:,3),'Right YH');
[MeanYH(8,1), StdYH(8,1)]=meanStd(globM_YH(:,3),'Right YH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\n OH SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nNormality for MEANS ALL OH\n\n');
[hJBallOH, hLillieallOH]=normality(globM_OH(:,1),'ALL OH');
[MeanOH(1,1), StdOH(1,1)]=meanStd(globM_OH(:,1),'ALL OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LH OH\n\n');
[hJBlhOH, hLillielhOH]=normality(glob2_OH(:,4),'LH OH');
[MeanOH(2,1), StdOH(2,1)]=meanStd(glob2_OH(:,4),'LH OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HH OH\n\n');
[hJBhhOH, hLilliehhOH]=normality(glob2_OH(:,1),'HH OH');
[MeanOH(3,1), StdOH(3,1)]=meanStd(glob2_OH(:,1),'HH OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HLH OH\n\n');
[hJBwhlhOH, hLilliewhlhOH]=normality(glob3_OH(:,1),'HLH OH');
[MeanOH(4,1), StdOH(4,1)]=meanStd(glob3_OH(:,1),'HLH OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LD OH\n\n');
[hJBldOH, hLillieldOH]=normality(glob6_OH,'LD OH');
[MeanOH(5,1), StdOH(5,1)]=meanStd(glob6_OH,'LD OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HD OH\n\n');
[hJBhdOH, hLilliehdOH]=normality(glob4_OH,'HD OH');
[MeanOH(6,1), StdOH(6,1)]=meanStd(glob4_OH,'HD OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Left OH\n\n');
[hJBLeftOH, hLillieLeftOH]=normality(globM_OH(:,2),'Left OH');
[MeanOH(7,1), StdOH(7,1)]=meanStd(globM_OH(:,2),'Left OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Right OH\n\n');
[hJBRightOH, hLillieRightOH]=normality(globM_OH(:,3),'Right OH');
[MeanOH(8,1), StdOH(8,1)]=meanStd(globM_OH(:,3),'Right OH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nEnd Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n\n');






% fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNormality for MEANS\n\n');
% [hJBallN hLillieallN]=normality(globM_YH(:,1),'allh N');
% [hJBwhN hLilliewhN]=normality(glob2_YH(:,1),'wh N');
% [hJBbhN hLilliebhN]=normality(glob2_YH(:,4),'bh N');
% [hJBwdN hLilliewdN]=normality(glob4_YH,'wd N');
% [hJBfN hLilliefN]=normality(globM_YH(:,5),'F N');
% [hJBeN hLillieeN]=normality(globM_YH(:,4),'E N');
% [hJBfN hLilliefN]=normality(globM_YH(:,2),'L N');
% [hJBeN hLillieeN]=normality(globM_YH(:,3),'R N');
% [hJBwhbhN hLilliewhbhN]=normality(glob3_YH(:,1),'wh-bh N');
% 
% 
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% [hJBallOH hLillieallOH]=normality(globM_OH(:,1),'allh OH');
% [hJBwhOH hLilliewhOH]=normality(glob2_OH(:,1),'wh OH');
% [hJBbhOH hLilliebhOH]=normality(glob2_OH(:,4),'bh OH');
% [hJBwdOH hLilliewdOH]=normality(glob4_OH,'wd OH');
% [hJBfOH hLilliefOH]=normality(globM_OH(:,5),'f OH');
% [hJBeOH hLillieeOH]=normality(globM_OH(:,4),'e OH');
% [hJBlOH hLillilOH]=normality(globM_OH(:,2),'L OH');
% [hJBrOH hLillierOH]=normality(globM_OH(:,3),'R OH');
% [hJBwhbhOH hLilliewhbhOH]=normality(glob3_OH(:,1),'wh-bh OH');

% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNormality for ALL\n\n');
% [hJBallN hLillieallN]=normality(globA_YH{9},'allh N');
% [hJBwhN hLilliewhN]=normality(globYH_YH{1},'wh N');
% [hJBbhN hLilliebhN]=normality(globYH_YH{4},'bh N');
% [hJBwdN hLilliewdN]=normality(globYHD_YH,'wd N');
% [hJBfN hLilliefN]=normality(globA_YH{4},'f N');
% [hJBeN hLillieeN]=normality(globA_YH{3},'e N');
% [hJBlN hLillielN]=normality(globA_YH{1},'L N');
% [hJBrN hLillierN]=normality(globA_YH{2},'R N');
% 
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% [hJBallOH hLillieallOH]=normality(globA_OH{9},'allh OH');
% [hJBwhOH hLilliewhOH]=normality(globYH_OH{1},'wh OH');
% [hJBbhOH hLilliebhOH]=normality(globYH_OH{4},'bh OH');
% [hJBwdOH hLilliewdOH]=normality(globYHD_OH,'wd OH');
% [hJBfOH hLilliefOH]=normality(globA_OH{4},'f OH');
% [hJBeOH hLillieeOH]=normality(globA_OH{3},'e OH');
% [hJBlOH hLillielOH]=normality(globA_OH{1},'L OH');
% [hJBrOH hLillierOH]=normality(globA_OH{2},'R OH');

