% Normality Tester, along with mean and std values %%%%%%%%%
run('MV/FilesReference');
out=evalc('Normlty_Testr()');
fid=fopen('StatsResults/Normlt/Normlt_Output_MV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('MV/FilesReference');
out=evalc('Homoskdstty_Testr()');
fid=fopen('StatsResults/Homosked/Homosked_Output_MV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('MV/FilesReference');
out=evalc('Mns_Testr()');
fid=fopen('StatsResults/Means/Means_Output_MV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('SDV/FilesReference');
out=evalc('Normlty_Testr()');
fid=fopen('StatsResults/Normlt/Normlt_Output_SDV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('SDV/FilesReference');
out=evalc('Homoskdstty_Testr');
fid=fopen('StatsResults/Homosked/Homosked_Output_SDV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('SDV/FilesReference');
out=evalc('Mns_Testr()');
fid=fopen('StatsResults/Means/Means_Output_SDV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('NVV/FilesReference');
out=evalc('Normlty_Testr()');
fid=fopen('StatsResults/Normlt/Normlt_Output_NVV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('NVV/FilesReference');
out=evalc('Homoskdstty_Testr()');
fid=fopen('StatsResults/Homosked/Homosked_Output_NVV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('NVV/FilesReference');
out=evalc('Mns_Testr()');
fid=fopen('StatsResults/Means/Means_Output_NVV.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('ETPv/FilesReference');
out=evalc('Normlty_Testr()');
fid=fopen('StatsResults/Normlt/Normlt_Output_ETPv.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('ETPv/FilesReference');
out=evalc('Homoskdstty_Testr()');
fid=fopen('StatsResults/Homosked/Homosked_Output_ETPv.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('ETPv/FilesReference');
out=evalc('Mns_Testr()');
fid=fopen('StatsResults/Means/Means_Output_ETPv.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('ETPy/FilesReference');
out=evalc('Normlty_Testr()');
fid=fopen('StatsResults/Normlt/Normlt_Output_ETPy.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('ETPy/FilesReference');
out=evalc('Homoskdstty_Testr()');
fid=fopen('StatsResults/Homosked/Homosked_Output_ETPy.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('ETPy/FilesReference');
out=evalc('Mns_Testr()');
fid=fopen('StatsResults/Means/Means_Output_ETPy.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run('ETPyRot/FilesReference');
out=evalc('Normlty_Testr()');
fid=fopen('StatsResults/Normlt/Normlt_Output_ETPyRot.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('ETPyRot/FilesReference');
out=evalc('Homoskdstty_Testr()');
fid=fopen('StatsResults/Homosked/Homosked_Output_ETPyRot.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;

run('ETPyRot/FilesReference');
out=evalc('Mns_Testr()');
fid=fopen('StatsResults/Means/Means_Output_ETPyRot.txt', 'wt');
fprintf(fid,'%s\n\nOVER AND OUT!!\n\n',out);
fclose(fid);
clear all;
