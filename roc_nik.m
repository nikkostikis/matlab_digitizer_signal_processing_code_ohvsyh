% wrapper for roc_machine
% it saves the output from roc_machine to the appropriate folder (lbl)

function roc_nik(pd, h, lbl)

% clear all;
% clc;

% wh_bh; %creates pd, h

ROC_out=roc_machine([pd, ones(size(pd)); h, zeros(size(h))],0,0.05,1,lbl);

fname = strcat('ROCs/ROC_', lbl, '.mat');

save(fname,'ROC_out');
