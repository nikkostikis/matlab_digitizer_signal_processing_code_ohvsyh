function ks2(A,B, column)


A=deNaNize(A);
B=deNaNize(B);

fprintf('\nKolmogorov-Smirnov Two-Sample test: h=1 if means are NOT the same\n');

[ks2h,ks2p] = ranksum(A,B,'alpha',0.01)
if ks2h==1
    fprintf('KS says %s means not equal\n',column);
else
    fprintf('KS says %s means equal\n', column);
end  
ks2p
fprintf('end of Kolmogorov-Smirnov test\n\n');


% fprintf('\n2-sample Kolmogorov-Smirnov test: h=1 if data not from same distribution\n');
% hKS = kstest2(A,B)
% if hKS==1
%     fprintf('KS says %s distributions not same\n', column);
% else
%     fprintf('KS says %s distributions same\n', column);
% end  

