function traj = yTrajectoryCorrection(x, y)
close all;

figure(1);hold on;

plot(x,y,'b');

if x(end)<x(1)
    xx = wrev(x);
    yy = wrev(y);
else
    xx = x;
    yy = y;
end

x1=xx(1);x2=xx(end);y1=yy(1);y2=yy(end);

hold on;
plot(x1,y1,'or');
plot(x2,y2,'xr');

hold on; 
plot(linspace(x1,x2,numel(x)), y1*ones(numel(x),1), '-k');

hold on;
plot([x1,x2], [y1,y2], '-r');

% solve linear system with a,b unknown for [x,y] start and end
A = [x1 1;x2 1];
b = [y1;y2];
k = inv(A)*b;

% calculate angle using trigonometry, or see the next comment
opposite = y2-y1;
adjucent = x2-x1;
tanTheta = opposite / adjucent;
theta = atan(tanTheta); 
% or just theta = atan(k(1));

% start of line needs to be translated to 0,0
origin = [x1;y1]; 
trajectoryCols = [xx';yy'];
for j=1:numel(x)
    trajectoryCols00(:,j) = trajectoryCols(:,j) - origin;
end

% cos and sin need theta in radians - clockwise rotation
% If theta > 0 I need clockwise rotation, so negative theta
% If theta < 0 I need counterclockwise rotation 
theta = -theta;
transfMatrix = [cos(theta) -sin(theta);sin(theta) cos(theta)];
for j=1:numel(x)
    trajectoryColsRot00(:,j) = transfMatrix * trajectoryCols00(:,j);
end

% translate back to start of line from 0,0
for j=1:numel(x)
    trajectoryColsRot(:,j) = trajectoryColsRot00(:,j) + origin;
end

plot(trajectoryColsRot(1,:)',trajectoryColsRot(2,:)', 'g');

traj = trajectoryColsRot';