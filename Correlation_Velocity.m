data=[H Velocity_H'];
[r p]=corrcoef(data)

r =

    1.0000    0.0679
    0.0679    1.0000


p =

    1.0000    0.0747
    0.0747    1.0000

data=[PD Velocity_PD'];
[r p]=corrcoef(data)

r =

    1.0000   -0.0781
   -0.0781    1.0000


p =

    1.0000    0.0093
    0.0093    1.0000