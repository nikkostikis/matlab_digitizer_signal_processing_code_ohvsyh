%%ALL VALUES

% load 'ProcessedData/AllDataN.mat'     %cell allData_H{25x12} 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.WH 11.BH 12.WD
% load 'ProcessedData/AllDataPD.mat'     %cell allData_PD{35x12} 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.WH 11.BH 12.WD
% 
% load 'ProcessedData/SHindexH_ALL.mat'   %cell globH_H -->all data Worst Hand x4
% load 'ProcessedData/SHindexPD_ALL.mat' %cell globH_PD -->all data Worst Hand x4
% 
% load 'ProcessedData/SHDindexH_ALL.mat'  %array globHD_H -->all data Worst Direction x1
% load 'ProcessedData/SHDindexPD_ALL.mat' %array globHD_PD -->all data Worst Direction x1
% 
% load 'ProcessedData/SindexH_ALL.mat'   %cell globA_H -->all data 11 cells 1.L 2.R 3.E 4.F 5.LE 6.LF 7.RE 8.RF 9.L+R 10.EF 11.R+LF
% load 'ProcessedData/SindexPD_ALL.mat'  %cell globA_PD -->all data 11 cells


% load 'ProcessedData/AllData_H.mat'     %cell allData_H{25x12} 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.WH 11.BH 12.WD
% load 'ProcessedData/AllData_PD.mat'     %cell allData_PD{35x12} 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.WH 11.BH 12.WD
% 
% load 'ProcessedData/WorstHand_H_ALL.mat'   %cell globH_H -->all data Worst Hand x4
% load 'ProcessedData/WorstHand_PD_ALL.mat' %cell globH_PD -->all data Worst Hand x4
% 
% load 'ProcessedData/WorstHandDirection_H_ALL.mat'  %array globHD_H -->all data Worst Direction x1
% load 'ProcessedData/WorstHandDirection_PD_ALL.mat' %array globHD_PD -->all data Worst Direction x1
% 
% load 'ProcessedData/H_ALL.mat'   %cell globA_H -->all data 11 cells 1.L 2.R 3.E 4.F 5.LE 6.LF 7.RE 8.RF 9.L+R 10.EF 11.R+LF
% load 'ProcessedData/PD_ALL.mat'  %cell globA_PD -->all data 11 cells



%%MEAN VALUES

% load 'ProcessedData/SindexH_MEANS.mat'     %matrix globM_H -->mean data 12 columns 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.EF 11.R+LF 12.AllWithoutWD
% load 'ProcessedData/SindexPD_MEANS.mat'    %matrix globM_PD -->mean data 12 columns
% 
% load 'ProcessedData/SHindexH_MEANS.mat'     %matrix glob2_H -->mean data Worst Hand x4 (WH,WE,WF,BH)
% load 'ProcessedData/SHindexPD_MEANS.mat'    %matrix glob2_PD -->mean data Worst Hand x4
% 
% load 'ProcessedData/DiafHindexH_MEANS.mat'  %matrix glob3_H -->mean data Differences WH-BH x3 (WH-BH, E(WH-BH), F(WH-BH))
% load 'ProcessedData/DiafHindexPD_MEANS.mat'  %matrix glob3_PD -->mean data Differences WH-BH x3
% 
% load 'ProcessedData/SHDindexH_MEANS.mat'    %array glob4_H -->mean data Worst Direction x1
% load 'ProcessedData/SHDindexPD_MEANS.mat'   %array glob4_PD -->mean data Worst Direction x1
% 
% load 'ProcessedData/DiafHDindexH_MEANS.mat' %array glob5_H -->Mean values, WorstHand Direction-BestHand Direction x1
% load 'ProcessedData/DiafHDindexPD_MEANS.mat' %array glob5_PD-->Mean values, WorstHand Direction-BestHand Direction x1
% 
% load 'ProcessedData/SBDindexH_MEANS.mat'    %array glob6_H -->mean data Best Direction x1
% load 'ProcessedData/SBDindexPD_MEANS.mat'   %array glob6_PD -->mean data Best Direction x1

load 'ProcessedData/H_MEANS.mat'     %matrix globM_H -->mean data 12 columns 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.EF 11.R+LF 12.AllWithoutWD
load 'ProcessedData/PD_MEANS.mat'    %matrix globM_PD -->mean data 12 columns

load 'ProcessedData/WorstHand_H_MEANS.mat'     %matrix glob2_H -->mean data Worst Hand x4 (WH,WE,WF,BH)
load 'ProcessedData/WorstHand_PD_MEANS.mat'    %matrix glob2_PD -->mean data Worst Hand x4

load 'ProcessedData/DiafWorstBestHand_H_MEANS.mat'  %matrix glob3_H -->mean data Differences WH-BH x3 (WH-BH, E(WH-BH), F(WH-BH))
load 'ProcessedData/DiafWorstBestHand_PD_MEANS.mat'  %matrix glob3_PD -->mean data Differences WH-BH x3

load 'ProcessedData/WorstHandDirection_H_MEANS.mat'    %array glob4_H -->mean data Worst Direction x1
load 'ProcessedData/WorstHandDirection_PD_MEANS.mat'   %array glob4_PD -->mean data Worst Direction x1

load 'ProcessedData/DiafWorstBestHandDirection_H_MEANS.mat' %array glob5_H -->Mean values, WorstHand Direction-BestHand Direction x1
load 'ProcessedData/DiafWorstBestHandDirection_PD_MEANS.mat' %array glob5_PD-->Mean values, WorstHand Direction-BestHand Direction x1

load 'ProcessedData/BestDirection_H_MEANS.mat'    %array glob6_H -->mean data Best Direction x1
load 'ProcessedData/BestDirection_PD_MEANS.mat'   %array glob6_PD -->mean data Best Direction x1





